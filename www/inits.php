<?php

// Composer autoloads
require_once 'vendor/autoload.php';

// CONFIG ENV
require_once 'config/env.php';
require_once 'config/' . $ENV . '/config.php';

// doctrine inits
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$config = Setup::createAnnotationMetadataConfiguration(array(), $devMode);
$em = EntityManager::create($dbParams, $config);

//require_once 'app/helpers/doctrine2-blob.php';

// twig view
require_once 'vendor/slim/slim/Slim/View.php';
require_once 'vendor/slim/extras/Views/TwigView.php';

TwigView::$twigDirectory = dirname(__FILE__) . '/vendor/Twig';
TwigView::$twigExtensions = array(
	'Twig_Extension_Debug',
    'Twig_Extensions_Slim',
	'Twig_YearMoDaHoMiSe_Extension',
);
TwigView::$twigOptions  = array(
	'charset' => 'utf-8',
	'auto_reload' => true,
    'cache' => false,#'tmp',
);

// 
$app = new Slim(array(
	'debug' => $devMode,
    'view' => 'TwigView',	
	'templates.path' => 'app/views',	
));

//
require_once 'app/loads.php';

?>