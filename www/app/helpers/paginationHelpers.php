<?php

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

function paginate(Query $query, $page, $limit, $fetchJoinCollection = true) {
    if(!$page && !$limit){
        return $query->getResult();
    }

    $offset = ($page * $limit) - $limit;

    $query->setFirstResult($offset);
    $query->setMaxResults($limit);

    $paginator = new Paginator($query, $fetchJoinCollection);

    $count = $paginator->count();

    $pagination_info = array(
        'total' => (int) $count,
        'current_page' => (int) $page,
        'total_pages' => (int) ceil($count / $limit)
    );
	
	/*
	var_dump($pagination_info);
	exit;
	*/

    return array(
        'pagination' => $paginator, // could just return $paginator->getIterator()
        'pagination_info' => $pagination_info,
		'page_numbers' => page_numbers1(
			$pagination_info['current_page'],
			$pagination_info['total_pages'], 10),
    );
}




function page_numbers1($cur, $max, $nItems) {
	$currentPageBlock = null;
	$startPage = null;
	$endPage = null;

	if(intval($cur) == 1 && intval($max) == 1) {
		return array('1');
	}

	$currentPageBlock = ceil($cur / $nItems);

	$startPage = ($currentPageBlock - 1) * $nItems + 1;
	$endPage = $startPage + $nItems - 1;

	if($startPage >= $max) $startPage = $max;
	if($endPage >= $max) $endPage = $max;
	
	$pagedList = array();
	for($i = $startPage; $i <= $endPage; $i ++){
		array_push($pagedList, $i);
	}

	return $pagedList;
}


?>