<?php



function mail_utf8($from, $to, $subject, $message, $cc='', $bcc='', $content_type='text/plain'){
	$from = explode("<",$from );
	
	$headers = "From: =?UTF-8?B?" .base64_encode($from[0])."?= <". $from[1] . "\n";

	$to = explode("<",$to );
	$to = "=?UTF-8?B?".base64_encode($to[0])."?= <". $to[1] ;

	$subject="=?UTF-8?B?".base64_encode($subject)."?=\n";

	if($cc!='')
	{
		$cc = explode("<",$cc );
		$headers .= "Cc: =?UTF-8?B?".base64_encode($cc[0])."?= <". $cc[1] . "\n";
	}

	if($bcc!='')
	{
		$bcc = explode("<",$bcc );
		$headers .= "Bcc: =?UTF-8?B?".base64_encode($bcc[0])."?= <". $bcc[1] . "\n";
	}

	$headers .=
		"Content-Type: ".$content_type."; "
		. "charset=UTF-8; format=flowed\n"
		. "MIME-Version: 1.0\n"
		. "Content-Transfer-Encoding: 8bit\n"
		. "X-Mailer: PHP\n";

	return mail($to, $subject, $message, $headers);
}


?>