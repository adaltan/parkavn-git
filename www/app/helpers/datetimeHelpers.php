<?php

function nowYearMoDaHoMiSe(){
    return strftime('%Y%m%d%H%M%S');
}


function fmtYearMoDaHoMiSe($s){
	return sprintf("%s/%s/%s %s:%s:%s",
		substr($s, 0, 4),
		substr($s, 4, 2),
		substr($s, 6, 2),
		substr($s, 8, 2),
		substr($s, 10, 2),
		substr($s, 12, 2));
}



class Twig_YearMoDaHoMiSe_Extension extends Twig_Extension {
    public function getName() {
        return 'yearmodahomise';
    }

    public function getFunctions() {
        return array(
            'fmtYearMoDaHoMiSe' => new Twig_Function_Method($this, 'fmtYearMoDaHoMiSe_'),
			'getFirstAttachmentId' => new Twig_Function_Method($this, 'getFirstAttachmentId'),
        );
    }
	
	public function fmtYearMoDaHoMiSe_($s) {
		return fmtYearMoDaHoMiSe($s);
	}
	
	
	public function getFirstAttachmentId($postId) {
		global $em;
		$a = $em->createQuery("SELECT a.id FROM Attachment a WHERE a.post = :post_id")
					->setParameter('post_id', $postId)
					->getResult();		
		if(count($a) >= 1){
			return $a[0]['id'];
		}else{
			return null;
		}
	}
	
}


?>
