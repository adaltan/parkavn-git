<?php


function applyHooks($app){
	$app->applyHook('view.session.binding');
	$app->applyHook('view.login.binding');
}


function render($app, $viewName, $data = array()){
    applyHooks($app);    
	$app->view()->appendData($data);
	$app->render($viewName);
}




function jsAlertAndBack($app, $msg){
	render($app, 'util/jsAlertAndBack.html', array('msg' => $msg));
}

function jsAlertAndGo($app, $msg, $url){
	render($app, 'util/jsAlertAndGo.html', array('msg' => $msg, 'url' => $url));
}


?>
