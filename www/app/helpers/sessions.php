<?php

session_cache_limiter(false);
session_start();



$app->hook('view.session.binding', function(){
	global $app;
	$view = $app->view();
	$view->appendData(array(
		'session' => $_SESSION,
	));
}, 1);



function setLogined($member){
    $_SESSION['login'] = $member;
}

function clearLogined(){
    $_SESSION['login'] = null;
}

function getLoginedOrNull(){
    try{
        return $_SESSION['login'];
    }catch(Exception $exc){
        return null;
    }
}


$app->hook('view.login.binding', function(){
    global $app;
    $view = $app->view();
    $view->appendData(array(
        'login' =>  getLoginedOrNull(),
        'login_admin'   => adminLogined() != null,
        'login_staff'   => staffLogined() != null,
    ));
}, 1);




function loginRequiredPage($app){
    $m = getLoginedOrNull();
    if($m){
        return $m;
    }else{ 
        jsAlertAndBack($app, '로그인 후 이용바랍니다.');
        return null;
    }
}


function adminLogined(){
    $m = getLoginedOrNull();
    if($m && $m->admin_yn == 'Y'){
        return $m;
    }else{
        return null;
    }
}


function adminLoginRequiredPage($app){
    if($m = adminLogined()){
        return $m;
    }else{
        jsAlertAndBack($app, '관리자 로그인 후 이용바랍니다.');
        return null;
    }
}


function staffLogined(){
    $m = getLoginedOrNull();
    if($m && ($m->admin_yn == 'Y' || $m->staff_yn == 'Y')){
        return $m;
    }else{
        return null;
    }
}


function staffLoginRequiredPage($app){
    if($m = staffLogined()){
        return $m;
    }else{
        jsAlertAndBack($app, '관리자/Staff 로그인 후 이용바랍니다.');
        return null;
    }
}


function signedUsersOnly($app){
	$m = getLoginedOrNull();
	if($m && ($m->join_type == 'TWITTER' || $m->join_type == 'FACEBOOK')){
		jsAlertAndBack($app, 'Twitter/Facebook에서 처리바랍니다.');
		return false;
	}else{
		return true;
	}
}



//EOF