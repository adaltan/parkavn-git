<?php
// -*- utf-8; -*-


// 회원 가입
$app->get('/member/agrees', function()use($app){
	render($app, 'member/agrees.html');
});



$app->get('/member/registForm', function()use($app){
	render($app, 'member/registForm.html');
});



function countRegisteredMemberId($memberId){
	global $em;
	$query = $em->createQuery('SELECT COUNT(m.id) FROM Member m WHERE m.id = ?1');
	$query->setParameter(1, $memberId);
	return 	$query->getSingleScalarResult();
}

$app->get('/member/countRegisteredMemberId/:memberId', function($memberId)use($app, $em){
	echo countRegisteredMemberId($memberId);
});


function countRegisteredMemberEmail($email){
	global $em;
	$query = $em->createQuery('SELECT COUNT(m.id) FROM Member m WHERE m.email = ?1');
	$query->setParameter(1, $email);
	return 	$query->getSingleScalarResult();
}



$app->get('/member/countRegisteredMemberEmail/:email', function($email)use($app, $em){
	echo countRegisteredMemberEmail($email);
});



$app->post('/member/registAction', function()use($app, $em){
	$form = $app->request()->post();
	//echo var_dump($form);
	// 아이디 체크
	if(strlen($form['id']) < 1 || countRegisteredMemberId($form['id']) > 0){
		return jsAlertAndBack($app, '아이디를 확인해주세요.');
	}
	// 이메일 체크
	if(strlen($form['email']) < 1 || countRegisteredMemberEmail($form['email']) > 0){
		return jsAlertAndBack($app, '이메일을 확인해주세요.');
	}
	// 필수항목 체크
	$requiredForms = array(
		'name'	=>	'회원 성명을 입력해주세요.',
		'email_rcpt_yn'	=>	'이메일 수신여부를 체크해주세요.',
		'public_profile_yn'	=>	'프로필 공개여부를 체크해주세요.',
		'memberIdChecked'	=>		'회원 아이디 중복 여부를 체크해주세요.',
		'memberEmailChecked'	=>	'회원 이메일 중복 여부를 체크해주세요.',
	);
	foreach($requiredForms as $k => $v){
		if(strlen($form[$k])<1){
			return jsAlertAndBack($app, $v);
		}
	}
	// 삽입
	$m = new Member();
	$m->id = $form['id'];
	$m->pw = passwordChecksum($form['pw']); // NOTE: no-raw-password!
	$m->email = $form['email'];
	$m->name = $form['name'];
	$m->email_rcpt_yn = $form['email_rcpt_yn'];
	$m->public_profile_yn = $form['public_profile_yn'];
	$m->birth_year = $form['birth_year'];
	$m->birth_month = $form['birth_month'];
	$m->birth_day = $form['birth_day'];
	try{
		$m->birth_luna_yn = $form['birth_luna_yn'];
	}catch(Exception $exc){}
	$em->persist($m);
	$em->flush();
	// 메인으로 이동.
	return jsAlertAndGo($app, '가입 완료하였습니다.', '/');
});


// 로그인
$app->get('/member/loginForm', function()use($app, $em){
    render($app, 'member/login.html');
});


$app->post('/member/loginAction', function()use($app, $em){
    $form = $app->request()->post();
    // find-user?
	$m = $em->find('Member', $form['id']);
	//
    if($m){
        // check-password?
		$shaed = passwordChecksum($form['pw']);
        if($shaed == $m->pw){
            // login
            setLogined($m);
            return $app->redirect('/');
        }else{
            return jsAlertAndBack($app, '회원 비밀번호가 일치하지 않습니다.');
        }
    }else{
        return jsAlertAndBack($app, '회원 아이디를 확인해주세요.');
    }
});



// 로그아웃
$app->get('/member/logout', function()use($app, $em){
    clearLogined();
    return $app->redirect('/');
});




// 회원 아이디 찾기
$app->get('/member/findIdForm', function()use($app, $em) {
	render($app, 'member/findIdForm.html');
});


$app->post('/member/findIdAction', function()use($app, $em) {
	// 회원 찾기
	$form = $app->request()->post();
	$m = $em->getRepository('Member')->findOneBy(array(
		'email' => $form['email'],
		'name'	=> $form['name'],
	));
	if($m){
		$from = sprintf("관리자 <parkavn@parkavn.cafe24.com>");
		$to = sprintf("%s <%s>", $m->name, $m->email);
		$title = 'PARK AVENUE 회원 아이디'; 
		$msg = sprintf('회원님의 PARK AVENUE 아이디는 다음과 같습니다. [%s]', $m->id);
		try{
			mail_utf8($from, $to, $title, $msg);
		}catch(Exception $exc){
			echo(sprintf("WARN: SENDMAIL FAILED: FIND-ID = [%s]", $m->id));
		}
		//render($app, 'member/findIdDone.html');		
		echo json_encode(array('found'=>true, 'id'=>$m->id));
	}else{
		echo json_encode(array('found'=>false, 'id'=>null));
		//jsAlertAndBack($app, '등록되지 않은 이메일 혹은 등록되지 않은 회원입니다.');
	}
});




//  회원 비밀번호 찾기
$app->get('/member/findPwForm', function()use($app, $em) {
	render($app, 'member/findPwForm.html');
});


$app->get('/member/findPwAction', function()use($app, $em) {
	// 회원 찾기
	$form = $app->request()->get();
	$m = $em->getRepository('Member')->findOneBy(array(
		'email' => $form['email'],
		'id'	=> $form['id'],
		'name'	=> $form['name'],
	));
	//
	$new_pw = sprintf('%s', mt_rand());
	if($m){
		// 비밀번호 리셋
		$m->pw = passwordChecksum($new_pw);
		$em->persist($m);
		$em->flush();
		// 리셋한 비밀번호 이메일로 보내기	
		$from = sprintf("관리자 <parkavn@parkavn.cafe24.com>");
		$to = sprintf("%s <%s>", $m->name, $m->email);
		$title = 'PARK AVENUE 임시 비밀번호';
		$msg = sprintf('회원님의 PARK AVENUE 임시 비밀번호는 다음과 같습니다. [%s]', $new_pw);
		try{
			mail_utf8($from, $to, $title, $msg);		
		}catch(Exception $exc){
			echo(sprintf("WARN: SENDMAIL FAILED: NEW PASSWORD = [%s]", $new_pw));
		}
		//
		//jsAlertAndBack($app, sprintf('임시 비밀번호를 이메일(%s)으로 보냈습니다.', $m->email));
		//render($app, 'member/findPwDone.html');
		echo json_encode(array('found'=>true, 'id'=>$form['id'], 'new_pw'=>$new_pw));		
	}else{
		echo json_encode(array('found'=>false, 'id'=>null, 'new_pw'=>null));
		//jsAlertAndBack($app, '등록되지 않은 회원입니다.');
	}
});







//  회원 프로필 수정
$app->get('/member/editProfileForm', function() use($app) {
	if(loginRequiredPage($app)){
		if(signedUsersOnly($app)){
			return render($app, 'member/editProfileForm.html');
		}
	}
});

$app->post('/member/editProfileAction', function() use($app, $em){
	$form = $app->request()->post();	
	if($m = loginRequiredPage($app)){
		$m = $em->find('Member', $m->id);
		//
		$requiredForms = array(
			'name'	=>	'회원 성명을 입력해주세요.',		
			'email_rcpt_yn'	=>	'이메일 수신여부를 체크해주세요.',
			'public_profile_yn'	=>	'프로필 공개여부를 체크해주세요.',
			'password' => '새로운 비밀번호를 입력해주세요.',
		);
		foreach($requiredForms as $k => $v){
			if(strlen($form[$k])<1){
				return jsAlertAndBack($app, $v);
			}
		}		
		//
		$m->name = $form['name'];
		$m->email_rcpt_yn = $form['email_rcpt_yn'];
		$m->public_profile_yn = $form['public_profile_yn'];
		$m->birth_year = $form['birth_year'];
		$m->birth_month = $form['birth_month'];
		$m->birth_day = $form['birth_day'];
		try{
			$m->birth_luna_yn = $form['birth_luna_yn'];
		}catch(Exception $exc){}
		$em->persist($m);
		$em->flush();	
		//
		$m->pw = passwordChecksum($form['password']);
		$em->persist($m);
		$em->flush();			
		//
		setLogined($m);
		//
		return jsAlertAndGo($app, '수정하였습니다.', '/member/editProfileForm');
	}
});





// 회원 비밀번호 변경
$app->get('/member/changePasswordForm', function() use($app) {
	if(loginRequiredPage($app)){
		if(signedUsersOnly($app)){
			return render($app, 'member/changePasswordForm.html');
		}
	}
});


$app->post('/member/changePasswordAction', function() use($app, $em) {
	$form = $app->request()->post();	
	if($m = loginRequiredPage($app)){
		$m = $em->find('Member', $m->id);
		if(passwordChecksum($form['old_pw']) == $m->pw){
			$m->pw = passwordChecksum($form['pw']);
			$em->persist($m);
			$em->flush();
			return jsAlertAndGo($app, '변경하였습니다.', '/');
		}else{
			return jsAlertAndBack($app, '기존 비밀번호가 일치하지 않습니다.');		
		}
	}
});



// twitter

$app->get('/member/twitter_login', function() use($app, $em) {
	$l = $_SESSION['TWITTER_LOGIN'];

	$id = $l->screen_name . '@TWITTER';
	
	$email = $id;
	$pw = '';
	$name = $l->name;
	$join_type = 'TWITTER';

	// find-or-not?
	$m = $em->find('Member', $id);
	if(!$m){
		$m = new Member();
		$m->id = $id;
		$m->email = $email;
		$m->pw = $pw;
		$m->name = $name;
		$m->join_type = $join_type;
		$em->persist($m);
		$em->flush();
		$m = $em->find('Member', $id);
	}
	// login
	setLogined($m);
	return $app->redirect('/');
});



// facebook

$app->get('/member/facebook_login', function() use($app, $em) {
	$l = $_SESSION['FACEBOOK_LOGIN'];

	$id = $l['username'] . '@FACEBOOK';
	
	$email = $id;
	$pw = '';
	$name = $l['name'];
	$join_type = 'FACEBOOK';

	// find-or-not?
	$m = $em->find('Member', $id);
	if(!$m){
		$m = new Member();
		$m->id = $id;
		$m->email = $email;
		$m->pw = $pw;
		$m->name = $name;
		$m->join_type = $join_type;
		$em->persist($m);
		$em->flush();
		$m = $em->find('Member', $id);
	}
	// login
	setLogined($m);
	return $app->redirect('/');
});







//EOF