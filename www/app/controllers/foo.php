<?php
$app->get('/foo', function() use($app, $em) {
	$log = $app->getLog();
	$log->debug('oh hai!');

	$query = $em->createQuery('SELECT COUNT(m.id) FROM Member m');
	$count = $query->getSingleScalarResult();

	$app->flashNow('info', 'Your credit card is expired');		
	$_SESSION['foo'] = '42';

	render($app, 'foo/foo.html', array('count' => $count ));
	
	//jsAlertAndGo($app, '가입 완료하였습니다.', '/');
});


$app->get('/foo_select', function()use($app, $em){
	$m = $em->find("Member", 'ageldama');	
	//var_dump($m);
	echo $m->id;
	$m->pw = 'foobar';
	$em->persist($m);
	$em->flush();
});



$app->get('/foo/sendmail-utf8', function()use($app, $em){
	mail_utf8('내가 <ageldama@gmail.com>', '내게 <ageldama@gmail.com>', '안녕?', '하이~');
	mail_utf8('내가 <ageldama@gmail.com>', '내게 <ageldama@naver.com>', '안녕?', '하이~');
});




$app->get('/foo/create-test-board/:name'
    ,function($name) use($app, $em){
        $b = new Board();
        $b->id = $name;
        $b->title = "TEST BOARD";
        $em->persist($b);
        $em->flush();
    });


$app->get('/foo/board/:board_id',
    function($board_id) use($app, $em){
        $b = $em->find('Board', $board_id);
        render($app, 'foo/board.html', array('board' => $b));
    });
	
	
	
$app->get('/foo/post/:post_id',
    function($post_id) use($app, $em){
        $p = $em->find('Post', $post_id);
        render($app, 'foo/post.html', array('post' => $p));
    });	


$app->get('/foo/add_comment/:post_id',
    function($post_id) use($app, $em){
        $p = $em->find('Post', $post_id);
        $c = new Comment();
		$c->member = $em->find('Member', 'ageldama');
		$c->ctime = '20120801010101';
		$c->contents = 'foobar';
		$c->post = $p;
		$em->persist($c);
		$em->flush();
    });


$app->get('/foo/strftime', function(){
	echo nowYearMoDaHoMiSe();
});


$app->get('/foo/fmtYearMoDaHoMiSe', function(){
	echo fmtYearMoDaHoMiSe(nowYearMoDaHoMiSe());
});



$app->get('/foo/page_numbers', function()use($app){
	$form = $app->request()->get();
	// cur?
	$cur = intval(dget($form, 'cur', '1'));
	// max?
	$max = intval(dget($form, 'max', '1'));
	//
	$p = page_numbers1($cur, $max, 10);
	//
	render($app, 'foo/page_numbers.html', 
		array(
			'cur'	=> $cur,
			'max'	=> $max,
			'p'		=> $p,
		));
});




$app->get('/foo/save-blob', function() use($app, $em){
	$p = $em->find('Post', 17);
	$m = $em->find('Member', 'ageldama');
	$a = new Attachment();
	$a->post = $p;
	$a->member = $m;
	$a->ctime = nowYearMoDaHoMiSe();
	$a->file_name = '1328088147242.jpg';
	$a->file_mime = 'image/jpeg';
	$a->file_size = filesize('c:/w/1328088147242.jpg');
	$a->file_blob = file_get_contents('c:/w/1328088147242.jpg');
	$em->persist($a);
	$em->flush();
});






$app->get('/foo/delete-attachment', function() use($app, $em){
	$v = $em->find('Attachment', ' ');
	if($v){$em->remove($v);}
	$em->flush();
});







$app->get('/foo/add-sns-types', function() use($app, $em){
	$p = $em->find('Post', '2');
	
	$cyworld = $em->find('SnsType', 'cyworld');
	$p->addSnsType($cyworld);
	
	$facebook = $em->find('SnsType', 'facebook');
	$p->addSnsType($facebook);

	$a = array(
		$cyworld->id	=> array(
			'title'	=> $cyworld->title,
			'desc'	=> 'CYCYCYCYCY',
		),
		$facebook->id	=> array(
			'title'	=> $facebook->title,
			'desc'	=> 'FBFBFBFB',
		),
	);
	
	PostSnsTypeMapDesc::saveOfPostId($em, $p->id, $a);
});



$app->get('/foo/view-sns-types', function() use($app, $em){
	//$p = $em->find('Post', '1');
	$p = $em->createQuery("SELECT p, m FROM Post p JOIN p.member m WHERE p.id = :post_id")
			->setParameter('post_id', '2')
			->getOneOrNullResult();		
	$st2 = PostSnsTypeMapDesc::listOfPostId($em, $p->id);	
	echo json_encode($st2);
});





$app->get('/foo/sns-types/:post_id', function($post_id) use($app, $em){
	//
	$p = $em->find('Post', $post_id);
	$snsTypes = $em->createQuery('SELECT st FROM SnsType st')->getResult();
	echo json_encode($snsTypes);
	//
	/*
	render($app, 'foo/sns-types/view.html', array(
		'post'		=> $p,
		'snsTypes'	=> $snsTypes,
	));
	*/
});






// TODO: post -> sns-types/save/:post_id





$app->get('/foo/jsons', function() use($app, $em){
	json_encode('             ');
	json_decode('                        ');
});








?>
