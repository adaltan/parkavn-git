<?php



$app->get('/admin/members', function() use($app, $em){
	if(adminLoginRequiredPage($app)){
		$form = $app->request()->get();
		//
		$qb = $em->createQueryBuilder();
		$qb->select(array('m'))
			->from('Member', 'm')			
			->where('m.id IS NOT NULL');
		//
		$searchTerm = dget($form, 'search', '');
		$searchWheres = array();
		$searchParams = array();
		if(dget($form, 'by_id', '') == 'Y'){
			array_push($searchWheres, 'm.id LIKE :id_like');
			$searchParams['id_like'] = '%' . $searchTerm . '%';
		}		
		if(dget($form, 'by_name', '') == 'Y'){
			array_push($searchWheres, 'm.name LIKE :name_like');
			$searchParams['name_like'] = '%' . $searchTerm . '%';
		}		
		if(dget($form, 'by_email', '') == 'Y'){
			array_push($searchWheres, 'm.email LIKE :email_like');
			$searchParams['email_like'] = '%' . $searchTerm . '%';
		}
		//
		if(count($searchWheres)>0){
			$qb->andWhere(implode(' OR ', $searchWheres));
		}
		foreach($searchParams as $k=>$v){
			$qb->setParameter($k, $v);
		}
		//
		$qb->orderBy('m.id', 'ASC');
		$curpage = intval(dget($form, 'curpage', 1));
		$p = paginate($qb->getQuery(), $curpage, 10);
        render($app, 'admin/members.html',
            array(
				'pagination' 		=> $p['pagination'],
				'pagination_info' 	=> $p['pagination_info'],
				'page_numbers'		=> $p['page_numbers'],
				'by_id'				=> dget($form, 'by_id', ''),
				'by_name'			=> dget($form, 'by_name', ''),
				'by_email'			=> dget($form, 'by_email', ''),
				'search'			=> dget($form, 'search', ''),
                'curpage'           => $curpage,
			));		
		//
		
	}
});





$app->get('/admin/editMemberForm/:member_id', function($member_id) use($app, $em){
    if($m = adminLoginRequiredPage($app)){
		$m = $em->find('Member', $member_id);
        render($app, 'admin/editMemberForm.html', array(
			'member'	=>	$m,
		));
    }
});




$app->post('/admin/editMemberAction/:member_id', function($member_id) use($app, $em){
    if($m = adminLoginRequiredPage($app)){
		$form = $app->request()->post();
		$m = $em->find('Member', $member_id);
		//
		$m->admin_yn = dget($form, 'admin_yn', '');
		$m->staff_yn = dget($form, 'staff_yn', '');
		//
		$em->persist($m);
		$em->flush();
		$app->redirect('/admin/editMemberForm/' . $m->id);
    }
});



$app->get('/admin/deleteMemberAction/:member_id', function($member_id) use($app, $em){
    if($m = adminLoginRequiredPage($app)){
		// 첨부 삭제
		$em->createQuery('DELETE Attachment a WHERE a.member = :member_id')
			->setParameter('member_id', $member_id)
			->execute();		
		// 댓글 삭제
		$em->createQuery('DELETE Comment a WHERE a.member = :member_id')
			->setParameter('member_id', $member_id)
			->execute();				
		// 게시글 삭제
		$em->createQuery('DELETE Post a WHERE a.member = :member_id')
			->setParameter('member_id', $member_id)
			->execute();		
		// 회원 삭제
		$em->createQuery('DELETE Member a WHERE a.id = :member_id')
			->setParameter('member_id', $member_id)
			->execute();			
		//
		$app->redirect('/admin/members');
	}
});



?>