<?php

$app->get('/admin/boards', function() use($app, $em){
	if(adminLoginRequiredPage($app)){
		$qb = $em->createQueryBuilder();
		$boards = $qb->select(array('b'))
			->from('Board', 'b')
			->getQuery()->getResult();
			render($app, 'admin/boards.html', array(
				'boards'	=> $boards,
			));
	}
});



$app->get('/admin/editBoardForm/:board_id', function($board_id) use($app, $em){
    if($m = adminLoginRequiredPage($app)){
		$b = $em->find('Board', $board_id);
        render($app, 'admin/editBoardForm.html', array(
			'board'	=>	$b,
		));
    }
});


$app->post('/admin/editBoardAction/:board_id', function($board_id) use($app, $em){
    if($m = adminLoginRequiredPage($app)){
		$form = $app->request()->post();
		$b = $em->find('Board', $board_id);
		$b->title = dget($form, 'title', '');
		$b->board_skin = dget($form, 'board_skin', '');
		$b->admin_post_yn = dget($form, 'admin_post_yn', '');
		$b->admin_comment_yn = dget($form, 'admin_comment_yn', '');
		$b->admin_view_yn = dget($form, 'admin_view_yn', '');
		$em->persist($b);
		$em->flush();
		$app->redirect('/admin/editBoardForm/' . $b->id);
    }
});



$app->get('/admin/sortPosts/:board_id', function($board_id) use($app, $em){
    if($m = adminLoginRequiredPage($app)){
		$form = $app->request()->get();
		$board = $em->find('Board', $board_id);
		$posts = $em->createQueryBuilder()
			->select(array('p', 'b',))
			->from('Post', 'p')
			->innerJoin('p.board', 'b')
			->where('p.board = :board_id')
			->setParameter('board_id', $board->id)
			->orderBy('p.ctime', 'DESC')
			->getQuery()->getResult();
        render($app, 'admin/sortPosts.html', array(
			'board'	=>	$board,
			'posts'	=>	$posts,
		));
	}
});

function dttm_1sec($d, $method){
	$dt = new DateTime();
	$parsed_time = strptime($d, '%Y%m%d%H%M%S');	
	$dt->setDate(1900 + $parsed_time['tm_year'],  1 + $parsed_time['tm_mon'], $parsed_time['tm_mday']);
	$dt->setTime($parsed_time['tm_hour'], $parsed_time['tm_min'], $parsed_time['tm_sec']);
	$interval = new DateInterval('PT1S');
	if($method == 'add'){
		$dt->add($interval);
	}elseif($method == 'sub'){
		$dt->sub($interval);
	}
	return $dt->format('YmdHis');
}

$app->get('/admin/movePostSortOrder', function() use($app, $em){
	/*if(true){
		echo json_encode(array('ok' => false, 'errmsg' => '제한된 기능입니다.',));
		exit;
	}*/
	//
	if($m = adminLoginRequiredPage($app)){
		$form = $app->request()->get();
		$board_id = dget($form, 'board_id', null);
		$moved_post_id = dget($form, 'movedId', null);
		$base_post_id = dget($form, 'baseId', null);
		$method = dget($form, 'method', 'sub');
		// STEP-01: 일단 base을 구하고
		$base_post = $em->find('Post', $base_post_id);
		// STEP-02: moved을 구하고
		$moved_post = $em->find('Post', $moved_post_id);
		// STEP-03: moved의 ctime을 base의 ctime에서 1초 뺀 값으로 업데이트
		$moved_post->ctime = dttm_1sec($base_post->ctime, $method);
		$em->persist($moved_post);
		$em->flush();
		//
		echo json_encode(array('ok' => true,));
	}	
});




?>