<?php



function boardSkinName($b, $filename) {
	if(strlen($b->board_skin)>0){
		return sprintf('board/skins/%s/%s', $b->board_skin, $filename);
	}else{
		return 'board/' . $filename;
	}
}


$app->get('/board/view/:board_id/:post_id',
    function($board_id, $post_id) use($app, $em) {
        // 게시판
        $b = $em->find('Board', $board_id);
		// 게시물
        $p = $em->createQuery("SELECT p, m FROM Post p JOIN p.member m WHERE p.id = :post_id")
                ->setParameter('post_id', $post_id)
                ->getOneOrNullResult();
		if($b->admin_view_yn == 'Y'){
			// staff이상도 아니고, 자신의 글도 아닐때.
			$m = loginRequiredPage($app);
			if(!$m) return;
			if($m->id != $p->member->id && staffLogined() == null){
				return jsAlertAndBack($app, '작성자나 스태프 이상 등급만 열람 가능합니다.');
			}			
		}
		// 관련 코멘트
        $c = $em->createQuery("SELECT c, m FROM Comment c JOIN c.member m WHERE c.post = :post_id ORDER BY c.id ASC")
            ->setParameter('post_id', $post_id)
            ->getResult();
		// 관련 첨부
        $a = $em->createQuery("SELECT a.id, a.file_size, a.file_mime, a.file_name FROM Attachment a WHERE a.post = :post_id")
            ->setParameter('post_id', $post_id)
            ->getResult();
		// SNS타입들
		$st = PostSnsTypeMapDesc::listOfPostId($em, $p->id);
		// update read-count
		$p->read_count ++;
		$em->persist($p);
		$em->flush();
		//
		$form = $app->request()->get();
		$curpage = dget($form, 'curpage', '');
        //
        render($app, boardSkinName($b, 'view.html'),
            array(
                'board'     =>  $b,
                'post'      =>  $p,
                'comments'  =>  $c,
                'attachments'   =>  $a,
				'curpage'	=>	$curpage,
				'sns_types'	=> $st,
            ));
    });


	
$app->get('/board/deletePost/:board_id/:post_id',
	function($board_id, $post_id) use($app, $em) {
		if($m = loginRequiredPage($app)){
			// 권한체크
			$p = $em->createQuery("SELECT p, m FROM Post p JOIN p.member m WHERE p.id = :post_id")
					->setParameter('post_id', $post_id)
					->getOneOrNullResult();			
			if($m->id != $p->member->id && staffLogined() == null){
				return jsAlertAndBack($app, '작성자나 스태프 이상 등급만 삭제 가능합니다.');
			}		
			// 관련 첨부, 댓글 삭제
			$em->createQuery('DELETE Comment c WHERE c.post = :post_id')
				->setParameter('post_id', $post_id)
				->execute();
			$em->createQuery('DELETE Attachment a WHERE a.post = :post_id')
				->setParameter('post_id', $post_id)
				->execute();			
			// SNS Types
			$em->getConnection()->exec(sprintf("DELETE FROM post_sns_type_map_desc WHERE post_id = '%s'", $post_id));
			$em->getConnection()->exec(sprintf("DELETE FROM post_sns_type_map WHERE post_id = '%s'", $post_id));
			// 게시글 삭제
			$em->createQuery('DELETE Post p WHERE p.id = :post_id')
				->setParameter('post_id', $post_id)
				->execute();				
			//
			return jsAlertAndGo($app, '삭제하였습니다.', '/board/list/'.$board_id);
		}
	});

	
	

?>
