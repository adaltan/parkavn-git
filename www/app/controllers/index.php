<?php

/*
$app->get('/', function(){
	global $app;
	applyHooks($app);
	$app->render('underconstruction.html');
});
*/


$app->get('/', function()use($app){
	render($app, 'index.html');
});

$app->get('/index2', function()use($app){
	render($app, 'index2.html');
});

$app->get('/a/:cat/:page', function($cat, $page)use($app){render($app, $cat . '/' . $page . '.html');});
?>