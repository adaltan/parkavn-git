<?php



$app->get('/board/editForm/:board_id/:post_id',
	function($board_id, $post_id) use($app, $em) {
		if($m = loginRequiredPage($app)){
			$b = $em->find('Board', $board_id);
			$p = $em->createQuery("SELECT p, m FROM Post p JOIN p.member m WHERE p.id = :post_id")
					->setParameter('post_id', $post_id)
					->getOneOrNullResult();			
			//
			if($m->id != $p->member->id && staffLogined() == null){
				return jsAlertAndBack($app, '작성자나 스태프 이상 등급만 수정 가능합니다.');
			}
			//
			$a = $em->createQuery("SELECT a.id, a.file_size, a.file_mime, a.file_name FROM Attachment a WHERE a.post = :post_id")
				->setParameter('post_id', $post_id)
				->getResult();			
			//
			return render($app, boardSkinName($b, 'editForm.html'), array(
				'post'		=> $p,
				'board'		=> $b,
				'attachments'	=> $a,
				'snsTypes'		=> PostSnsTypeMapDesc::listOfPostId($em, $p->id),
				'snsTypeCodes'	=>	SnsType::listAll($em),				
				'snsTypeCodesJson'	=>	json_encode(SnsType::listAll($em)),				
			));
		}
	});
	
	
$app->post('/board/editAction/:board_id/:post_id',
	function($board_id, $post_id) use($app, $em) {
		$form = $app->request()->post();
		if($m = loginRequiredPage($app)){
			$b = $em->find('Board', $board_id);
			$p = $em->createQuery("SELECT p, m FROM Post p JOIN p.member m WHERE p.id = :post_id")
					->setParameter('post_id', $post_id)
					->getOneOrNullResult();			
			//
			if($m->id != $p->member->id && staffLogined() == null){
				return jsAlertAndBack($app, '작성자나 스태프 이상 등급만 수정 가능합니다.');
			}			
			//
			$m = $em->find('Member', $m->id);
			// 내용들?
			$p->title = dget($form, 'title', '');
			$p->contents = dget($form, 'contents', '');
			$p->var_1 = dget($form, 'var_1', '');
			$p->var_2 = dget($form, 'var_2', '');
			$p->var_3 = dget($form, 'var_3', '');
			// 새 첨부 추가
			foreach($_FILES as $f){
				if(is_uploaded_file($f['tmp_name'])){
					$a = new Attachment();
					$a->ctime = nowYearMoDaHoMiSe();
					$a->post = $p;
					$a->member = $m;
					$a->file_name = $f['name'];
					$a->file_mime = $f['type'];
					$a->file_size = $f['size'];
					$a->file_blob = file_get_contents($f['tmp_name']);
					$em->persist($a);
					$em->flush();
				}
			}
			// 첨부 지우기.
			foreach(explode(',', dget($form, 'del_attachments', '')) as $del_attachment){
				$conn = $em->getConnection();
				$conn->beginTransaction();
				$conn->exec(sprintf("DELETE FROM attachment WHERE id = '%s'", $del_attachment));
				$conn->commit();
			}
			// SNS
			$snsTypeMapDescJson = dget($form, 'snsTypeMapDesc', '');
			PostSnsTypeMapDesc::saveOfPostId($em, $p->id, json_decode($snsTypeMapDescJson, true));			
			// 입력 내용 검사후 저장.
			if(strlen($p->title) < 1){
				return jsAlertAndBack($app, '제목을 입력해주세요.');
			}else{
				$em->persist($p);
				$em->flush();
				return $app->redirect(sprintf('/board/list/%s', $board_id));
			}
		}
	});



?>