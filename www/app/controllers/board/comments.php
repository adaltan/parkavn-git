<?php

$app->post('/board/writeComment/:board_id/:post_id',
	function($board_id, $post_id) use($app, $em) {
		$form = $app->request()->post();
		$b = $em->find('Board', $board_id);
		if($m = loginRequiredPage($app)){
			if($b->admin_comment_yn == 'Y' ){
				$m = staffLoginRequiredPage($app);
				if($m == null) return;
			}
			//
			$m = $em->find('Member', $m->id);
			$p = $em->find('Post', $post_id);
			//
			$c = new Comment();
			$c->member = $m;
			$c->post = $p;
			$c->ctime = nowYearMoDaHoMiSe();
			$c->title = dget($form, 'title', '');
			$c->contents = dget($form, 'contents', '');			
			// 내용검사
			if(/*strlen($c->title) < 1 || */strlen($c->contents) < 1){
				return jsAlertAndBack($app, '내용을 입력해주세요.');
			}else{
				$em->persist($c);
				$em->flush();
				return $app->redirect('/board/list/' . $board_id);
			}
		}
	});



$app->get('/board/deleteComment/:board_id/:post_id/:comment_id',
	function($board_id, $post_id, $comment_id) use($app, $em) {
		if($m = loginRequiredPage($app)){
			// 권한 체크
			$c = $em->createQuery("SELECT c, m FROM Comment c JOIN c.member m WHERE c.id = :comment_id")
					->setParameter('comment_id', $comment_id)
					->getOneOrNullResult();			
			if($m->id != $c->member->id && staffLogined() == null){
				return jsAlertAndBack($app, '작성자나 스태프 이상 등급만 삭제 가능합니다.');
			}		
			//
			$em->createQuery('DELETE Comment c WHERE c.id = :comment_id')
				->setParameter('comment_id', $comment_id)
				->execute();				
			//
			return jsAlertAndGo($app, '삭제하였습니다.', '/board/list/'.$board_id);			
		}
	});


?>