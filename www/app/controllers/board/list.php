<?php



$app->get('/board/list/:board_id',
    function($board_id) use($app, $em){
		$form = $app->request()->get();
        // default query
		$qb = $em->createQueryBuilder();
		$qb->select(array('p', 'm', 'b',))
			->from('Post', 'p')
			->innerJoin('p.member', 'm')
			->innerJoin('p.board', 'b')
			->where('p.board = :board_id')
			->setParameter('board_id', $board_id);
		// search terms
		$searchTerm = dget($form, 'search', '');
		$searchWheres = array();
		$searchParams = array();
		if(dget($form, 'by_id', '') == 'Y'){
			array_push($searchWheres, 'm.id LIKE :id_like');
			$searchParams['id_like'] = '%' . $searchTerm . '%';
		}		
		if(dget($form, 'by_name', '') == 'Y'){
			array_push($searchWheres, 'm.name LIKE :name_like');
			$searchParams['name_like'] = '%' . $searchTerm . '%';
		}		
		if(dget($form, 'by_title', '') == 'Y'){
			array_push($searchWheres, 'p.title LIKE :title_like');
			$searchParams['title_like'] = '%' . $searchTerm . '%';
		}		
		if(dget($form, 'by_contents', '') == 'Y'){
			array_push($searchWheres, 'p.contents LIKE :contents_like');
			$searchParams['contents_like'] = '%' . $searchTerm . '%';
		}
		if(count($searchWheres)>0){
			$qb->andWhere(implode(' OR ', $searchWheres));
		}
		foreach($searchParams as $k=>$v){
			$qb->setParameter($k, $v);
		}
		// query closings: order by
		$qb->orderBy('p.ctime', 'DESC');
		//echo $qb->getDql();
		//
		$b = $em->find('Board', $board_id);
        //
        $curpage = intval(dget($form, 'curpage', 1));
		if($curpage<1)$curpage=1;
		$rows = 10;
		//
		$skip_pagings = array('staff_gallery');
		if(in_array($board_id, $skip_pagings)){
			$rows = 99999;
		}
		//
		$rows_per_pages = array(
			'starwedding'		=>	14,
			#'staff_gallery'		=>	12,
			'customer_gallery'	=>	14,
		);
		foreach($rows_per_pages 	as $k => $v){
			if($k == $board_id) $rows = $v;
		}
		//
		$p = paginate($qb->getQuery(), $curpage, $rows);	// page = 1..n
		$skin = 'list.html';
		if(dget($form, 'sub', '') == 'Y'){
			$skin = 'list_sub.html';
		}
		$post_id = dget($form, 'post_id', '');
        render($app, boardSkinName($b, $skin),
            array(
				'pagination' 		=> $p['pagination'],
				'pagination_info' 	=> $p['pagination_info'],
				'page_numbers'		=> $p['page_numbers'],
				'by_id'				=> dget($form, 'by_id', ''),
				'by_name'			=> dget($form, 'by_name', ''),
				'by_title'			=> dget($form, 'by_title', ''),
				'by_contents'		=> dget($form, 'by_contents', ''),
				'search'			=> dget($form, 'search', ''),
                'board_id'			=> $board_id,
                'curpage'           => $curpage,
				'post_id'			=> $post_id,
			));
    });


?>