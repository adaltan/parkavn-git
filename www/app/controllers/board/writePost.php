<?php


$app->get('/board/writeForm/:board_id',
    function($board_id) use($app, $em){
        $b = $em->find('Board', $board_id);
		if($m = loginRequiredPage($app)){            
			if($b->admin_post_yn == 'Y' ){
				$m = staffLoginRequiredPage($app);
				if($m == null) return;
			}
			//
			render($app, boardSkinName($b, 'writeForm.html'),
				array(
					'board'		=>	$b,
					'snsTypeCodes'	=>	SnsType::listAll($em),
					'snsTypeCodesJson'	=>	json_encode(SnsType::listAll($em)),
				));			
		}
    });
	
	
$app->post('/board/writeAction/:board_id',
	function($board_id) use($app, $em){
		$form = $app->request()->post();
		$b = $em->find('Board', $board_id);
		if($m = loginRequiredPage($app)){
			if($b->admin_post_yn == 'Y' ){
				$m = staffLoginRequiredPage($app);
				if($m == null) return;
			}
			//
			$m = $em->find('Member', $m->id);
			$p = new Post();
			$p->board = $b;
			$p->ctime = nowYearMoDaHoMiSe();
			$p->title = dget($form, 'title', '');
			$p->contents = dget($form, 'contents', '');
			$p->var_1 = dget($form, 'var_1', '');
			$p->var_2 = dget($form, 'var_2', '');
			$p->var_3 = dget($form, 'var_3', '');
			$p->read_count = 0;
			$p->member = $m;
			if(strlen($p->title) < 0){
				return jsAlertAndBack($app, '제목을 입력해주세요.');
			}
			//
			$em->persist($p);
			$em->flush();
			// 첨부 파일들
			foreach($_FILES as $f){
				if(is_uploaded_file($f['tmp_name'])){
					$a = new Attachment();
					$a->ctime = nowYearMoDaHoMiSe();
					$a->post = $p;
					$a->member = $m;
					$a->file_name = $f['name'];
					$a->file_mime = $f['type'];
					$a->file_size = $f['size'];
					$a->file_blob = file_get_contents($f['tmp_name']);
					$em->persist($a);
					$em->flush();
				}
			}
			// SNS
			$snsTypeMapDescJson = dget($form, 'snsTypeMapDesc', '');
			PostSnsTypeMapDesc::saveOfPostId($em, $p->id, json_decode($snsTypeMapDescJson, true));
			//
			//return $app->redirect('/board/list/' . $b->id . '/' . $p->id);
			return $app->redirect('/board/list/' . $b->id);
		};
	});

?>