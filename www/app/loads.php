<?php


$apps = array(
	## Models	
	'models/member.php',
	'models/board.php',
	'models/post.php',	
	'models/comment.php',
	'models/attachment.php',
	'models/sns_type.php',
	
	## Helpers
	'helpers/sessions.php',
	'helpers/viewHelpers.php',
	'helpers/passwordHelpers.php',
    'helpers/mailHelpers.php',
    'helpers/datetimeHelpers.php',
	'helpers/paginationHelpers.php',
	'helpers/miscHelpers.php',
	
	## Controllers
	'controllers/foo.php',
	'controllers/index.php',
	'controllers/member.php',
	'controllers/board.php',
	'controllers/board/list.php',
	'controllers/board/writePost.php',
	'controllers/board/editPost.php',
	'controllers/board/comments.php',
	'controllers/admin.php',
	'controllers/admin/board.php',
	'controllers/admin/member.php',
);

$apps_base = './app/';

foreach($apps as $i){
	require_once($apps_base . $i);
}

?>
