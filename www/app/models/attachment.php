<?php

/**
 * @Entity @Table(name="attachment")
 **/
class Attachment {
    /**
     * @Id @Column(name="id", type="integer")
	 * @generatedValue(strategy="AUTO")
     **/
	public $id;
	
	/**
	 * @ManyToOne(targetEntity="Post", inversedBy="attachments")
	 **/
	public $post;
	
	/**
	 * @ManyToOne(targetEntity="Member", inversedBy="attachments")
	 **/
	public $member;	
	
	/**
     * @Column(name="ctime", type="string")
     **/	
	public $ctime;

	/**
     * @Column(name="file_name", type="string")
     **/	
	public $file_name;

	/**
     * @Column(name="file_mime", type="string")
     **/	
	public $file_mime;

	/**
     * @Column(name="file_size", type="integer")
     **/	
	public $file_size;
	
	/**
     * @Column(name="file_blob", type="blob")
     **/	
	public $file_blob;

}




?>