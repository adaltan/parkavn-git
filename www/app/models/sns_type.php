<?php

/**
 * @Entity @Table(name="sns_type")
 **/
class SnsType {
    /**
     * @Id @Column(name="id", type="string")
     **/
	public $id;
	
	
	/**
	 * @Column(name="title", type="string")
	 **/
	public $title;

	
	/**
	 * @ManyToMany(targetEntity="Post", mappedBy="sns_types")
	 **/
 	private $posts;	
	


	public function __construct(){
        $this->posts = new \Doctrine\Common\Collections\ArrayCollection();
    }
	
	
	public function addPost($p){
		$this->posts[] = $p;
	}
	
	
	public static function listAll($em){
		return $em->createQuery('SELECT st FROM SnsType st')->getResult();		
	}
}






/**
 * @Entity @Table(name="post_sns_type_map_desc")
 **/
class PostSnsTypeMapDesc {
	/**
	 * @Id @Column(name="post_id", type="integer")
	 **/
	public $post_id;
	
	/**
	 * @Id @Column(name="sns_type_id", type="string")
	 **/
	public $sns_type_id;
	
	/**
	 * @Column(name="var_1", type="string")
	**/
	public $var_1;
	
	/** 관련된 SnsType의 배열을 다시 채워주기. */
	public static function findBy($em, $post, $snsTypes){
		$result = array();
		//
		foreach($snsTypes as $snsType){
		    $p = $em->createQuery("SELECT d FROM PostSnsTypeMapDesc d WHERE d.post_id = :post_id AND d.sns_type_id = :sns_type_id")
                ->setParameter('post_id', $post->id)
				->setParameter('sns_type_id', $snsType->id)
                ->getOneOrNullResult();
			if($p != null){
				$result[$snsType->id] = array(
					'title'		=> $snsType->title,
					'desc'		=> $p->var_1,
				);
			}else{
				$result[$snsType->id] = array(
					'title'		=> '',
					'desc'		=> '',
				);
			}
		}
		//
		return $result;
	}
	
	public static function listOfPostId($em, $postId){
		$post = $em->find('Post', $postId);
		return PostSnsTypeMapDesc::findBy($em, $post, $post->sns_types);
	}
	
	public static function saveOfPostId($em, $postId, $a){
		$conn = $em->getConnection();
		$conn->beginTransaction();
		// 일단 관련 sns-type-map이랑 desc 삭제.
		$conn->exec(sprintf("DELETE FROM post_sns_type_map_desc WHERE post_id = '%s'", $postId));
		$conn->exec(sprintf("DELETE FROM post_sns_type_map WHERE post_id = '%s'", $postId));		
		// map들 만들기
		$stmt_map = $conn->prepare('INSERT INTO post_sns_type_map (post_id, sns_type_id) VALUES (:post_id, :sns_type_id)');
		$stmt_desc = $conn->prepare('INSERT INTO post_sns_type_map_desc (post_id, sns_type_id, var_1) 
			VALUES (:post_id, :sns_type_id, :var_1)');
		if($a){
			foreach($a as $sns_type_id => $sns_type_map_and_desc){
				$stmt_map->execute(array(':post_id' => $postId, ':sns_type_id' => $sns_type_id));
				$stmt_desc->execute(array(':post_id' => $postId, ':sns_type_id' => $sns_type_id, 
					':var_1' => $sns_type_map_and_desc['desc']));
			}
		}
		//
		$conn->commit();
	}
}








?>