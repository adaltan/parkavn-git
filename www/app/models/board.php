<?php

/**
 * @Entity @Table(name="board")
 **/
class Board {

    /**
     * @Id @Column(name="id", type="string")
     * @var string
     **/
	public $id;
    
	/**
     * @Column(name="title", type="string")
     * @var string
     **/	
	public $title;

    /**
     * @Column(name="admin_post_yn", type="string")
     * @var string
     **/	
	public $admin_post_yn;

    /**
     * @Column(name="admin_comment_yn", type="string")
     * @var string
     **/	
	public $admin_comment_yn;
	
    /**
     * @Column(name="admin_view_yn", type="string")
     * @var string
     **/	
	public $admin_view_yn;
	
	/**
     * @Column(name="board_skin", type="string")
     **/	
	public $board_skin;
	
	
	/**
	 * @OneToMany(targetEntity="Post", mappedBy="board")
	**/
	public $posts;
	
}

?>
