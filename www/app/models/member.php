<?php


/**
 * @Entity @Table(name="member")
 **/
class Member
{
    /**
     * @Id @Column(name="id", type="string")
     * @var string
     **/
	public $id;

    /**
     * @Column(name="pw", type="string")
     * @var string
     **/
	public $pw;

    /**
     * @Column(name="name", type="string")
     * @var string
     **/
	public $name;

    /**
     * @Column(name="email", type="string")
     * @var string
     **/
	public $email;

    /**
     * @Column(name="email_rcpt_yn", type="string")
     * @var string
     **/
	public $email_rcpt_yn;

    /**
     * @Column(name="public_profile_yn", type="string")
     * @var string
     **/
	public $public_profile_yn;

    /**
     * @Column(name="admin_yn", type="string")
     * @var string
     **/
	public $admin_yn;

	/**
     * @Column(name="staff_yn", type="string")
     * @var string
     **/
	public $staff_yn;

	/**
     * @Column(name="birth_year", type="string")
     * @var string
     **/
	public $birth_year;

	/**
     * @Column(name="birth_month", type="string")
     * @var string
     **/
	public $birth_month;

    /**
     * @Column(name="birth_day", type="string")
     * @var string
     **/
	public $birth_day;

    /**
     * @Column(name="birth_luna_yn", type="string")
     * @var string
     **/
	public $birth_luna_yn;

    /**
     * @Column(name="join_type", type="string")
     * @var string
     **/
	public $join_type;

	
	
	/**
	 * @OneToMany(targetEntity="Post", mappedBy="member")
	**/
	public $posts;
	
	/**
	 * @OneToMany(targetEntity="Comment", mappedBy="member")
	**/
	public $comments;	
	
	/**
	 * @OneToMany(targetEntity="Attachment", mappedBy="member")
	**/
	public $attachments;		
}




?>	