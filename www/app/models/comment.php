<?php



/**
 * @Entity @Table(name="comment")
 **/
class Comment {
    /**
     * @Id @Column(name="id", type="integer")
	 * @generatedValue(strategy="AUTO")	 
     **/
	public $id;
	
	/**
	 * @ManyToOne(targetEntity="Post", inversedBy="comments")
	 **/
	public $post;
	
	/**
	 * @ManyToOne(targetEntity="Member", inversedBy="comments")
	 **/
	public $member;	

	/**
     * @Column(name="ctime", type="string")
     **/	
	public $ctime;

	/**
     * @Column(name="contents", type="string")
     **/	
	public $contents;
    

}
	

?>