<?php

/**
 * @Entity @Table(name="post")
 **/
class Post {
    /**
     * @Id @Column(name="id", type="integer")
	 * @generatedValue(strategy="AUTO")	 
     **/
	public $id;
    
	/**
	 * @ManyToOne(targetEntity="Board", inversedBy="posts")
	 **/
	public $board;
	
	/**
	 * @ManyToOne(targetEntity="Member", inversedBy="posts")
	 **/
	public $member;

    /**
     * @Column(name="title", type="string")
     **/	
	public $title;
	
    /**
     * @Column(name="ctime", type="string")
     **/		
	public $ctime;	// NOTE: type="datetime" -- ok.

    /**
     * @Column(name="read_count", type="integer")
     **/		
	public $read_count;

    /**
     * @Column(name="contents", type="string")
     **/		
	public $contents;
	
	
	/**
	 * @Column(name="var_1", type="string")
	 **/
	public $var_1;
	
	/**
	 * @Column(name="var_2", type="string")
	 **/
	public $var_2;
	
	/**
	 * @Column(name="var_3", type="string")
	 **/
	public $var_3;	
	

	/**
	 * @OneToMany(targetEntity="Comment", mappedBy="post")
	**/
	public $comments;	
	
	/**
	 * @OneToMany(targetEntity="Attachment", mappedBy="post")
	**/
	public $attachments;	
	
	

	/**
	 * @ManyToMany(targetEntity="SnsType", inversedBy="posts")
	 * @JoinTable(name="post_sns_type_map",
	 *      joinColumns={@JoinColumn(name="post_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@JoinColumn(name="sns_type_id", referencedColumnName="id")}
	 *      )
	 **/
	public $sns_types;		



	public function __construct(){
        $this->sns_types = new \Doctrine\Common\Collections\ArrayCollection();
    }	


	public function addSnsType($st){
		$this->sns_types[] = $st;
	}
	
}

	

?>
