/*
-- Query: select * from board
LIMIT 0, 1000

-- Date: 2012-08-15 21:18
*/
INSERT INTO `board` (`id`,`title`,`admin_post_yn`,`admin_comment_yn`,`board_skin`,`admin_view_yn`) VALUES ('free','자유게시판',NULL,NULL,'free',NULL);
INSERT INTO `board` (`id`,`title`,`admin_post_yn`,`admin_comment_yn`,`board_skin`,`admin_view_yn`) VALUES ('notice','공지사항','Y','Y','notice',NULL);
INSERT INTO `board` (`id`,`title`,`admin_post_yn`,`admin_comment_yn`,`board_skin`,`admin_view_yn`) VALUES ('reservation','상담예약게시판',NULL,NULL,'reservation','Y');
INSERT INTO `board` (`id`,`title`,`admin_post_yn`,`admin_comment_yn`,`board_skin`,`admin_view_yn`) VALUES ('staff_gallery','스탭 갤러리','Y','Y','staff_gallery',NULL);
INSERT INTO `board` (`id`,`title`,`admin_post_yn`,`admin_comment_yn`,`board_skin`,`admin_view_yn`) VALUES ('starwedding','스타웨딩 갤러리','Y','Y','star_gallery',NULL);


insert into board (id, title, board_skin) values('customer_gallery', 'CUSTOMER GALLERY', 'customer_gallery');
