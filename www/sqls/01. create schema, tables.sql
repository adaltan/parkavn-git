SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `parkavn` DEFAULT CHARACTER SET utf8 ;
USE `parkavn` ;

-- -----------------------------------------------------
-- Table `parkavn`.`member`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `parkavn`.`member` (
  `ID` VARCHAR(200) NOT NULL ,
  `PW` VARCHAR(200) NOT NULL ,
  `NAME` VARCHAR(200) NOT NULL ,
  `EMAIL` VARCHAR(200) NOT NULL ,
  `EMAIL_RCPT_YN` VARCHAR(10) NULL DEFAULT NULL ,
  `PUBLIC_PROFILE_YN` VARCHAR(10) NULL DEFAULT NULL ,
  `ADMIN_YN` VARCHAR(10) NULL DEFAULT NULL ,
  `STAFF_YN` VARCHAR(10) NULL DEFAULT NULL ,
  `BIRTH_YEAR` VARCHAR(10) NULL DEFAULT NULL ,
  `BIRTH_MONTH` VARCHAR(10) NULL DEFAULT NULL ,
  `BIRTH_DAY` VARCHAR(10) NULL DEFAULT NULL ,
  `BIRTH_LUNA_YN` VARCHAR(10) NULL DEFAULT NULL ,
  `JOIN_TYPE` VARCHAR(10) NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  UNIQUE INDEX `EMAIL_UNIQUE` (`EMAIL` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `parkavn`.`board`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `parkavn`.`board` (
  `id` VARCHAR(200) NOT NULL ,
  `title` VARCHAR(200) NOT NULL ,
  `admin_post_yn` VARCHAR(10) NULL DEFAULT NULL ,
  `admin_comment_yn` VARCHAR(10) NULL DEFAULT NULL ,
  `board_skin` VARCHAR(200) NULL DEFAULT NULL ,
  `admin_view_yn` VARCHAR(10) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `parkavn`.`post`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `parkavn`.`post` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `board_id` VARCHAR(200) NOT NULL ,
  `member_id` VARCHAR(200) NOT NULL ,
  `title` VARCHAR(200) NOT NULL ,
  `ctime` VARCHAR(20) NOT NULL ,
  `read_count` INT NOT NULL DEFAULT 0 ,
  `contents` LONGTEXT NOT NULL ,
  `var_1` VARCHAR(200) NULL ,
  `var_2` VARCHAR(200) NULL ,
  `var_3` VARCHAR(200) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_post_board` (`board_id` ASC) ,
  INDEX `fk_post_member` (`member_id` ASC) ,
  CONSTRAINT `fk_post_board`
    FOREIGN KEY (`board_id` )
    REFERENCES `parkavn`.`board` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_post_member`
    FOREIGN KEY (`member_id` )
    REFERENCES `parkavn`.`member` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `parkavn`.`comment`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `parkavn`.`comment` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `post_id` INT NOT NULL ,
  `ctime` VARCHAR(20) NOT NULL ,
  `contents` LONGTEXT NOT NULL ,
  `member_id` VARCHAR(200) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_comment_post` (`post_id` ASC) ,
  INDEX `fk_comment_member` (`member_id` ASC) ,
  CONSTRAINT `fk_comment_post`
    FOREIGN KEY (`post_id` )
    REFERENCES `parkavn`.`post` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_member`
    FOREIGN KEY (`member_id` )
    REFERENCES `parkavn`.`member` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `parkavn`.`attachment`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `parkavn`.`attachment` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `post_id` INT(11) NOT NULL ,
  `member_ID` VARCHAR(200) NOT NULL ,
  `ctime` VARCHAR(20) NOT NULL ,
  `file_name` VARCHAR(200) NOT NULL ,
  `file_mime` VARCHAR(200) NOT NULL ,
  `file_size` INT(11) NOT NULL ,
  `file_blob` LONGBLOB NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_attachment_post` (`post_id` ASC) ,
  INDEX `fk_attachment_member` (`member_ID` ASC) ,
  CONSTRAINT `fk_attachment_member`
    FOREIGN KEY (`member_ID` )
    REFERENCES `parkavn`.`member` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attachment_post`
    FOREIGN KEY (`post_id` )
    REFERENCES `parkavn`.`post` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `parkavn`.`sns_type`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `parkavn`.`sns_type` (
  `id` VARCHAR(200) NOT NULL ,
  `title` VARCHAR(200) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `parkavn`.`post_sns_type_map`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `parkavn`.`post_sns_type_map` (
  `post_id` INT NOT NULL ,
  `sns_type_id` VARCHAR(200) NOT NULL ,
  INDEX `fk_post_sns_type_map_sns_type1` (`sns_type_id` ASC) ,
  INDEX `fk_post_sns_type_map_post1` (`post_id` ASC) ,
  CONSTRAINT `fk_post_sns_type_map_post1`
    FOREIGN KEY (`post_id` )
    REFERENCES `parkavn`.`post` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_post_sns_type_map_sns_type1`
    FOREIGN KEY (`sns_type_id` )
    REFERENCES `parkavn`.`sns_type` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `parkavn`.`post_sns_type_map_desc`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `parkavn`.`post_sns_type_map_desc` (
  `var_1` VARCHAR(200) NULL ,
  `post_id` INT NOT NULL ,
  `sns_type_id` VARCHAR(200) NOT NULL ,
  UNIQUE INDEX `post_sns_type_map_desc_keys` (`post_id` ASC, `sns_type_id` ASC) ,
  INDEX `fk_post_sns_type_map_desc_post` (`post_id` ASC) ,
  INDEX `fk_post_sns_type_map_desc_sns_type` (`sns_type_id` ASC) ,
  CONSTRAINT `fk_post_sns_type_map_desc_post`
    FOREIGN KEY (`post_id` )
    REFERENCES `parkavn`.`post` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_post_sns_type_map_desc_sns_type`
    FOREIGN KEY (`sns_type_id` )
    REFERENCES `parkavn`.`sns_type` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
