<?php
// boardDownloadAttachment.php




function square_crop($src_image, $dest_image, $thumb_size = 64, $jpg_quality = 90) {
 
    // Get dimensions of existing image
    $image = getimagesize($src_image);
 
    // Check for valid dimensions
    if( $image[0] <= 0 || $image[1] <= 0 ) return false;
 
    // Determine format from MIME-Type
    $image['format'] = strtolower(preg_replace('/^.*?\//', '', $image['mime']));
 
    // Import image
    switch( $image['format'] ) {
        case 'jpg':
        case 'jpeg':
            $image_data = imagecreatefromjpeg($src_image);
        break;
        case 'png':
            $image_data = imagecreatefrompng($src_image);
        break;
        case 'gif':
            $image_data = imagecreatefromgif($src_image);
        break;
        default:
            // Unsupported format
            return false;
        break;
    }
 
    // Verify import
    if( $image_data == false ) return false;
 
    // Calculate measurements
    if( $image[0] & $image[1] ) {
        // For landscape images
        $x_offset = ($image[0] - $image[1]) / 2;
        $y_offset = 0;
        $square_size = $image[0] - ($x_offset * 2);
    } else {
        // For portrait and square images
        $x_offset = 0;
        $y_offset = ($image[1] - $image[0]) / 2;
        $square_size = $image[1] - ($y_offset * 2);
    }
 
    // Resize and crop
    $canvas = imagecreatetruecolor($thumb_size, $thumb_size);
	//
    if( imagecopyresampled(
        $canvas,
        $image_data,
        0,
        0,
        $x_offset,
        $y_offset,
        $thumb_size,
        $thumb_size,
        $square_size,
        $square_size
    ) ) {

		/*
		$white = imagecolorallocate($canvas, 0xff, 0xff, 0xff);
		imagefill($canvas, 0 , 0 , $white);
		imagefill($canvas, $thumb_size ,0, $white);
		*/
	
        // Create thumbnail
        switch( strtolower(preg_replace('/^.*\./', '', $dest_image)) ) {
            case 'jpg':
            case 'jpeg':
                return imagejpeg($canvas, $dest_image, $jpg_quality);
            break;
            case 'png':
                return imagepng($canvas, $dest_image);
            break;
            case 'gif':
                return imagegif($canvas, $dest_image);
            break;
            default:
                // Unsupported format
                return false;
            break;
        }
 
    } else {
        return false;
    }
 
}
 

function make_thumbnail($source_path, $width, $height, $thumbnail_path){
    list($img_width,$img_height, $type) = getimagesize($source_path);
    if ($type!=1 && $type!=2 && $type!=3 && $type!=15) return;
    if ($type==1) $img_sour = imagecreatefromgif($source_path);
    else if ($type==2 ) $img_sour = imagecreatefromjpeg($source_path);
    else if ($type==3 ) $img_sour = imagecreatefrompng($source_path);
    else if ($type==15) $img_sour = imagecreatefromwbmp($source_path);
    if ($img_width > $img_height) {
        $w = round($height*$img_width/$img_height);
        $h = $height;
        $x_last = round(($w-$width)/2);
        $y_last = 0;
    } else {
        $w = $width;
        $h = round($width*$img_height/$img_width);
        $x_last = 0;
        $y_last = round(($h-$height)/2);
    }
    if ($img_width < $width && $img_height < $height) {
        $img_last = imagecreatetruecolor($width, $height); 
        $x_last = round(($width - $img_width)/2);
        $y_last = round(($height - $img_height)/2);

        imagecopy($img_last,$img_sour,$x_last,$y_last,0,0,$w,$h);
        imagedestroy($img_sour);
        $white = imagecolorallocate($img_last,255,255,255);
        imagefill($img_last, 0, 0, $white);
    } else {
        $img_dest = imagecreatetruecolor($w,$h); 
        imagecopyresampled($img_dest, $img_sour,0,0,0,0,$w,$h,$img_width,$img_height); 
        $img_last = imagecreatetruecolor($width,$height); 
        imagecopy($img_last,$img_dest,0,0,$x_last,$y_last,$w,$h);
        imagedestroy($img_dest);
    }
    if ($thumbnail_path) {
        if ($type==1) imagegif($img_last, $thumbnail_path, 100);
        else if ($type==2 ) imagejpeg($img_last, $thumbnail_path, 100);
        else if ($type==3 ) imagepng($img_last, $thumbnail_path, 100);
        else if ($type==15) imagebmp($img_last, $thumbnail_path, 100);
    } else {
        if ($type==1) imagegif($img_last);
        else if ($type==2 ) imagejpeg($img_last);
        else if ($type==3 ) imagepng($img_last);
        else if ($type==15) imagebmp($img_last);
    }
    imagedestroy($img_last);
}



$id = $_GET['id'];
$thumbWH = $_GET['thumbWH'];


// CONFIG ENV
require_once 'config/env.php';
require_once 'config/' . $ENV . '/config.php';

// PDO Connection & Query
$dbh = new PDO(sprintf('mysql:host=%s;dbname=%s', $dbParams['host'], $dbParams['dbname']), $dbParams['user'], $dbParams['password']);

$stmt = $dbh->prepare("SELECT * FROM attachment WHERE id = :id");
$stmt->bindParam(':id', $_GET['id']);

$stmt->execute();
$row = $stmt->fetch();


$type = $row['file_mime'];
$name = $row['file_name'];
$size = $row['file_size'];
$data = $row['file_blob'];

//
if(false && $thumbWH){
	//
	$ext = pathinfo($name, PATHINFO_EXTENSION);
	//
	$fn = tempnam(sys_get_temp_dir(), 'thumb-') . '.' . $ext;
	$fp = fopen($fn, 'wb');
	fwrite($fp, $data);
	fclose($fp); 
	//
	$fn_dst = tempnam(sys_get_temp_dir(), 'thumb-') . '.' . $ext;
	$ret = true; make_thumbnail($fn, $thumbWH, $thumbWH, $fn_dst);
	//$ret = square_crop($fn, $fn_dst, $thumbWH);
	if(!ret){
		echo "CROP-THUMB FAIL!!!";
		exit;
	}
	//
	$flen2 = filesize ($fn_dst);
	$fp2 = fopen($fn_dst, 'rb');
	$data = fread($fp2, $flen2);
	$size = $flen2;
	fclose($fp2);	
	//
	unlink($fn);
	unlink($fn_dst);
}

//
header("Content-type: $type");
header("Content-length: $size");
header("Content-Disposition: filename=" . urlencode($name));
header("Content-Description: Server Generated Data");
echo $data;

//EOF